<?php
namespace App\Model;

use Nette;
use Nette\Security;

class Authenticator implements Security\IAuthenticator
{
	public $database;

	function __construct(Nette\Database\Context $database)
	{
		$this->database = $database;
	}

	function authenticate(array $credentials)
	{
		list($username, $password) = $credentials;
		$row = $this->database->table('user')
			->where('email', $username)->fetch();

		if (!$row) {
			throw new Security\AuthenticationException('Neznámý e-mail.');
		}

		if (!Security\Passwords::verify($password, $row->password)) {
			throw new Security\AuthenticationException('Neplatné heslo.');
		}

		$this->database->table('user')
			->where('id', $row->id)->update(array('last_login' => new Nette\Database\SqlLiteral('NOW()')));

		$arr = $row->toArray();
		unset($arr['password']);
		return new Security\Identity($row->id, $row->role, $arr);
	}
}