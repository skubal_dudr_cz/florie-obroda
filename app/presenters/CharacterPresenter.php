<?php

namespace App\Presenters;

use Nette;
use Nette\Application\UI\Form;


class CharacterPresenter extends BasePresenter
{
	protected function beforeRender()
	{
		if (!$this->getUser()->isLoggedIn()) {
			$this->flashMessage('Nejte přihlášen', 'info');
			$this->redirect('Homepage:default');
		}

		return parent::beforeRender();
	}

	protected function startup()
	{
		parent::startup();
		if (!$this->getUser()->isAllowed('backend')) {
			throw new Nette\Application\ForbiddenRequestException;
		}
	}

	public function renderList($order_column = 'created_at', $order_desc = true)
	{
		$this->template->order_column = $order_column;
		$this->template->order_desc = $order_desc;

		$this->template->characters = $this->database->table('character')
			->order($order_column . ' ' . ($order_desc ? 'DESC' : 'ASC'));
	}

	public function renderShow($characterId)
	{
		$character = $this->database->table('character')->get($characterId);
		if (!$character) {
			$this->error('Role nebyla nalezena!');
		}

		$this->template->character = $character;
	}

	public function actionCreate()
	{
	}

	public function actionEdit($characterId)
	{
		$this->template->characterId = $characterId;
		$character = $this->database->table('character')->get($characterId);
		if (!$character) {
			$this->error('Role nebyla nalezena');
		}
		$this['characterForm']->setDefaults($character->toArray());
	}

	public function actionDelete($characterId)
	{
		$this->template->characterId = $characterId;
		$character = $this->database->table('character')->get($characterId);
		if (!$character) {
			$this->error('Role nebyla nalezena');
		}
		$character->delete();
		$this->flashMessage('Role byla smazána', 'success');
		$this->redirect('list');
	}

	protected function createComponentCharacterForm()
	{
		if (!$this->getUser()->isLoggedIn()) {
			$this->error('Musíte se přihlásit');
		}

		$characterId = $this->getParameter('characterId');
		$oldUserId = $this->database->table('character')->wherePrimary($characterId)->fetchField('user_id');
		
		$form = new Form;
		$form->addText('name', 'Role / jméno:')
			->setRequired();

		$form->addTextArea('user_note', 'Pracovní poznámky (vyřešit do startu hry, netiskne se)');

		$form->addTextArea('user_characteristics', 'Popis role:')
			->setAttribute('class', 'mceEditor');

		$form->addTextArea('admin_note', 'Organizační shrnutí: (tiskne se organizátorům)');

		$options = array(null => '(není)');
		$query = $this->database->table('user')
			->joinWhere(':character', true)
			->where(':character.id IS NULL' . ($oldUserId ? ' OR user.id = ' . $oldUserId : ''));
		while ($row = $query->fetch()) {
			$options[$row->id] = $row->nickname . ' (' . $row->name . ')'. ($row->continuing ? ' - pokračování v roli':'');
		}
		if ($this->getUser()->isInRole('admin')) {
			$form->addSelect('user_id', 'Hráč:', $options);
		}

		$form->addSubmit('send', 'Uložit');
		$form->onSuccess[] = [$this, 'characterFormSucceeded'];

		$renderer = $form->getRenderer();
		$renderer->wrappers['controls']['container'] = 'dl';
		$renderer->wrappers['pair']['container'] = null;
		$renderer->wrappers['label']['container'] = 'dt';
		$renderer->wrappers['control']['container'] = 'dd';
		return $form;
	}


	public function characterFormSucceeded($form, $values)
	{
		if ($values['user_id'] == "") {
			$values['user_id'] = null;
		}

		$characterId = $this->getParameter('characterId');
		if ($characterId) {
			$character = $this->database->table('character')->get($characterId);
			$character->update($values);
		} else {
			$character = $this->database->table('character')->insert($values);
		}

		$this->flashMessage('Role byla uložena', 'success');
		$this->redirect('show', $character->id);
	}
}
