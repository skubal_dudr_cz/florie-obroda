<?php

namespace App\Presenters;

use Nette;
use Nette\Application\UI\Form;


class TailPresenter extends BasePresenter
{
	protected function beforeRender()
	{
		if (!$this->getUser()->isLoggedIn()) {
			$this->flashMessage('Nejte přihlášen', 'info');
			$this->redirect('Homepage:default');
		}

		return parent::beforeRender();
	}

	protected function startup()
	{
		parent::startup();
		if (!$this->getUser()->isAllowed('backend')) {
			throw new Nette\Application\ForbiddenRequestException;
		}
	}

	public function renderList($order_column = 'created_at', $order_desc = true)
	{
		$this->template->order_column = $order_column;
		$this->template->order_desc = $order_desc;

		$this->template->tails = $this->database->table('tail')
			->group('tail.id')
			->order($order_column . ' ' . ($order_desc ? 'DESC' : 'ASC'));
	}

	public function renderShow($tailId)
	{
		$tail = $this->database->table('tail')->get($tailId);
		if (!$tail) {
			$this->error('Příběhová linka nebyla nalezena!');
		}

		$this->template->tail = $tail;
	}

	public function actionCreate()
	{
		if (!$this->getUser()->isLoggedIn()) {
			$this->redirect('Homepage:default');
		}
	}

	public function actionEdit($tailId)
	{
		if (!$this->getUser()->isLoggedIn()) {
			$this->redirect('Homepage:default');
		}

		$tail = $this->database->table('tail')->get($tailId);
		if (!$tail) {
			$this->error('Příběhová linka nebyla nalezena');
		}
		$this['tailForm']->setDefaults($tail->toArray());
		$this->template->tailId = $tailId;
	}

	public function actionDelete($tailId)
	{
		$this->template->tailId = $tailId;
		$tail = $this->database->table('tail')->get($tailId);
		if (!$tail) {
			$this->error('Příběhová linka nebyla nalezena');
		}
		$tail->delete();
		$this->flashMessage('Příběhová linka byla smazána', 'success');
		$this->redirect('list');
	}

	protected function createComponentTailForm()
	{
		if (!$this->getUser()->isLoggedIn()) {
			$this->error('Musíte se přihlásit');
		}

		$form = new Form;
		$form->addText('name', 'Název příběhové linky:')
			->setRequired();

		$form->addTextArea('description', 'Popis příběhové linky: (zobrazuje se všem členům)');

		$form->addTextArea('admin_note', 'Poznámka organizátora: (neviditelná)');

		$form->addSubmit('send', 'Uložit');
		$form->onSuccess[] = [$this, 'tailFormSucceeded'];

		$renderer = $form->getRenderer();
		$renderer->wrappers['controls']['container'] = 'dl';
		$renderer->wrappers['pair']['container'] = null;
		$renderer->wrappers['label']['container'] = 'dt';
		$renderer->wrappers['control']['container'] = 'dd';

		return $form;
	}


	public function tailFormSucceeded($form, $values)
	{
		$tailId = $this->getParameter('tailId');

		if ($tailId) {
			$tail = $this->database->table('tail')->get($tailId);
			$tail->update($values);
		} else {
			$tail = $this->database->table('tail')->insert($values);
		}

		$this->flashMessage('Příběhová linka byla uložena', 'success');
		$this->redirect('show', $tail->id);
	}

}
