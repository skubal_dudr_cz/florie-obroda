<?php

namespace App\Presenters;

use Nette;
use Nette\Application\UI\Form;


class TailCharacterPresenter extends BasePresenter
{
	protected function beforeRender()
	{
		if (!$this->getUser()->isLoggedIn()) {
			$this->flashMessage('Nejte přihlášen', 'info');
			$this->redirect('Homepage:default');
		}

		return parent::beforeRender();
	}

	protected function startup()
	{
		parent::startup();
		if (!$this->getUser()->isAllowed('backend')) {
			throw new Nette\Application\ForbiddenRequestException;
		}
	}

	public function actionCreate($characterId = null, $tailId = null)
	{
		if (!$this->getUser()->isLoggedIn()) {
			$this->redirect('Homepage:default');
		}

		$character = null;
		if ($characterId) {
			$character = $this->database->table('character')->get($characterId);
			if (!$character) {
				$this->error('Role nebyla nalezena');
			}
		}
		$this->template->character = $character;
		
		$tail = null;
		if ($tailId) {
			$tail = $this->database->table('tail')->get($tailId);
			if (!$tail) {
				$this->error('Příběhová linka nebyla nalezena');
			}
		}
		$this->template->tail = $tail;
	}

	public function actionEdit($xTailId, $characterId = null, $tailId = null)
	{
		if (!$this->getUser()->isLoggedIn()) {
			$this->redirect('Homepage:default');
		}

		$xTail = $this->database->table('tail_x_character')->get($xTailId);
		if (!$xTail) {
			$this->error('Zapojení do příběhové linky nebylo nalezeno');
		}
		
		$this->template->xTail = $xTail;
		
		$this['membershipForm']->setDefaults($xTail->toArray());
	}

	public function actionDelete($xTailId)
	{
		$this->template->xTailId = $xTailId;
		$xTail = $this->database->table('tail_x_character')->get($xTailId);
		if (!$xTail) {
			$this->error('Zapojení do příběhové linky nebylo nalezeno');
		}
		$tailId = $xTail->tail_id;
		$xTail->delete();
		$this->flashMessage('Role byla z příběhové linky vyřazena', 'success');
		$this->redirect('Tail:show', $tailId);
	}

	protected function createComponentMembershipForm()
	{
		
		if (!$this->getUser()->isLoggedIn()) {
			$this->error('Musíte se přihlásit');
		}

		$form = new Form;

		$xTailId = $this->getParameter('xTailId');
		$tailId = $this->getParameter('tailId');
		$characterId = $this->getParameter('characterId');

		if ($xTailId) {
			$xTail = $this->database->table('tail_x_character')->get($xTailId);
			$tailId = $xTail->tail_id;
			$characterId = $xTail->character_id;
		}

		if ($tailId) {
			$form->addHidden('tail_id', $tailId);
		} else {
			$form->addSelect('tail_id', 'Příběhová linka', $this->database->table('tail')
					->joinWhere(':tail_x_character', ':tail_x_character.character_id = '.$characterId)
					->where(':tail_x_character.id IS NULL')
					->fetchPairs('id', 'name'))
				->setRequired();
		}
		if ($characterId) {
			$form->addHidden('character_id', $characterId);
		} else {
			$form->addSelect('character_id', 'Role',$this->database->table('character')
					->joinWhere(':tail_x_character', ':tail_x_character.tail_id = '.$tailId)
					->where(':tail_x_character.id IS NULL')
					->fetchPairs('id', 'name'))
				->setRequired();
		}

		$form->addTextArea('caption', 'Nadpis pro konkrétního hráče: (zobrazuje se jen tomuto hráči)');
		$form->addTextArea('description', 'Text pro konkrétního hráče: (zobrazuje se jen tomuto hráči)');

		$form->addTextArea('admin_note', 'Poznámka organizátora: (nezobrazuje se)');

		$form->addSubmit('send', 'Uložit');
		$form->onSuccess[] = [$this, 'membershipFormSucceeded'];

		$renderer = $form->getRenderer();
		$renderer->wrappers['controls']['container'] = 'dl';
		$renderer->wrappers['pair']['container'] = null;
		$renderer->wrappers['label']['container'] = 'dt';
		$renderer->wrappers['control']['container'] = 'dd';

		return $form;
	}


	public function membershipFormSucceeded($form, $values)
	{
		$xTailId = $this->getParameter('xTailId');

		if ($xTailId) {
			$xTail = $this->database->table('tail_x_character')->get($xTailId);
			$xTail->update($values);
		} else {
			$xTail = $this->database->table('tail_x_character')->insert($values);
		}

		$this->flashMessage('Členství ve legendě bylo uloženo', 'success');
		if ($this->getParameter('tailId')) {
			$this->redirect('Tail:show', $xTail->tail_id);
		}
		$this->redirect('Character:show', $xTail->character_id);
	}

}
