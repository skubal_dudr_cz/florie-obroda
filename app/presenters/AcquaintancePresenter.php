<?php

namespace App\Presenters;

use Nette;
use Nette\Application\UI\Form;


class AcquaintancePresenter extends BasePresenter
{
	protected function beforeRender()
	{
		if (!$this->getUser()->isLoggedIn()) {
			$this->flashMessage('Nejte přihlášen', 'info');
			$this->redirect('Homepage:default');
		}

		return parent::beforeRender();
	}

	protected function startup()
	{
		parent::startup();
		if (!$this->getUser()->isAllowed('backend')) {
			throw new Nette\Application\ForbiddenRequestException;
		}
	}

	public function actionCreate($objectId = null, $characterId = null)
	{
		if (!$this->getUser()->isLoggedIn()) {
			$this->redirect('Homepage:default');
		}

		$object = null;
		if ($objectId) {
			$object = $this->database->table('character')->get($objectId);
			if (!$object) {
				$this->error('Role nebyla nalezena');
			}
		}
		$this->template->object = $object;
		
		$character = null;
		if ($characterId) {
			$character = $this->database->table('character')->get($characterId);
			if (!$character) {
				$this->error('Příběhová linka nebyla nalezena');
			}
		}
		$this->template->character = $character;
	}

	public function actionEdit($acquaintanceId, $objectId = null, $characterId = null)
	{
		if (!$this->getUser()->isLoggedIn()) {
			$this->redirect('Homepage:default');
		}

		$acquaintance = $this->database->table('acquaintance')->get($acquaintanceId);
		if (!$acquaintance) {
			$this->error('Zapojení do příběhové linky nebylo nalezeno');
		}
		
		$this->template->acquaintance = $acquaintance;
		
		$this['acquaintanceForm']->setDefaults($acquaintance->toArray());
	}

	public function actionDelete($acquaintanceId)
	{
		$this->template->acquaintanceId = $acquaintanceId;
		$acquaintance = $this->database->table('acquaintance')->get($acquaintanceId);
		if (!$acquaintance) {
			$this->error('Zapojení do příběhové linky nebylo nalezeno');
		}
		$characterId = $acquaintance->character_id;
		$acquaintance->delete();
		$this->flashMessage('Role byla z příběhové linky vyřazena', 'success');
		$this->redirect('Subject:show', $characterId);
	}

	protected function createComponentAcquaintanceForm()
	{
		
		if (!$this->getUser()->isLoggedIn()) {
			$this->error('Musíte se přihlásit');
		}

		$form = new Form;

		$acquaintanceId = $this->getParameter('acquaintanceId');
		$characterId = $this->getParameter('characterId');
		$objectId = $this->getParameter('objectId');

		if ($acquaintanceId) {
			$acquaintance = $this->database->table('acquaintance')->get($acquaintanceId);
			$characterId = $acquaintance->character_id;
			$objectId = $acquaintance->object_id;
		}

		if ($characterId) {
			$form->addHidden('character_id', $characterId);
		} else {
			$form->addSelect('character_id', 'Známost ke komu', $this->database->table('character')
					->joinWhere(':acquaintance', ':acquaintance.object_id = '.$objectId)
					->where(':acquaintance.id IS NULL')
					->fetchPairs('id', 'name'))
				->setRequired();
		}
		if ($objectId) {
			$form->addHidden('object_id', $objectId);
		} else {
			$form->addSelect('object_id', 'Role',$this->database->table('character')
					->joinWhere(':acquaintance', ':acquaintance.character_id = '.$characterId)
					->where(':acquaintance.id IS NULL AND character.id !='.$characterId)
					->fetchPairs('id', 'name'))
				->setRequired();
		}

		$form->addTextArea('description', 'Text: (zobrazuje se jen tomuto hráči, nezobrazuje se druhé straně)');

		$form->addTextArea('admin_note', 'Poznámka organizátora: (nezobrazuje se)');

		$form->addSubmit('send', 'Uložit');
		$form->onSuccess[] = [$this, 'acquaintanceFormSucceeded'];

		$renderer = $form->getRenderer();
		$renderer->wrappers['controls']['container'] = 'dl';
		$renderer->wrappers['pair']['container'] = null;
		$renderer->wrappers['label']['container'] = 'dt';
		$renderer->wrappers['control']['container'] = 'dd';

		return $form;
	}


	public function acquaintanceFormSucceeded($form, $values)
	{
		$acquaintanceId = $this->getParameter('acquaintanceId');

		if ($acquaintanceId) {
			$acquaintance = $this->database->table('acquaintance')->get($acquaintanceId);
			$acquaintance->update($values);
		} else {
			$acquaintance = $this->database->table('acquaintance')->insert($values);
		}

		$this->flashMessage('Členství ve legendě bylo uloženo', 'success');
		if ($this->getParameter('characterId')) {
			$this->redirect('Character:show', $acquaintance->character_id);
		}
		$this->redirect('Character:show', $acquaintance->character_id);
	}

}
