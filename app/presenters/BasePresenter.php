<?php

namespace App\Presenters;

use Nette;
use Nette\Application\UI\Form;


class BasePresenter extends Nette\Application\UI\Presenter
{
	/** @var Nette\Database\Context */
	protected $database;

	public function __construct(Nette\Database\Context $database)
	{
		$this->database = $database;
	}

	protected function startup()
	{
		parent::startup();
		if (!$this->getUser()->isLoggedIn() && $this->name != 'Homepage') {
			$this->redirect('Homepage:default');
		}

		$this->template->myCharacter = $this->database->table('character')->where('user_id', $this->getUser()->getId())->fetch();
	}

	protected function beforeRender()
	{
		$this->template->addFilter('bool', function ($bool, $yes = 'ano', $no = 'ne') {
			return $bool ? $yes : $no;
		});

		$this->template->presenter = $this->name;
		return parent::beforeRender();
	}
}
