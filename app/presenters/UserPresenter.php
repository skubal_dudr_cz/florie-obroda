<?php

namespace App\Presenters;

use Nette;
use Nette\Application\UI\Form;


class UserPresenter extends BasePresenter
{
	private $_role = array('player' => 'Hráč', 'admin' => 'Organizátor');

	protected function beforeRender()
	{
		if (!$this->getUser()->isLoggedIn()) {
			$this->flashMessage('Nejte přihlášen', 'info');
			$this->redirect('Homepage:default');
		}

		$this->template->addFilter('role', function ($role) {
			return $this->_role[$role];
		});

		return parent::beforeRender();
	}

	public function renderList($order_column = 'created_at', $order_desc = true)
	{
		if (!$this->getUser()->isAllowed('backend')) {
			throw new Nette\Application\ForbiddenRequestException;
		}

		$this->template->order_column = $order_column;
		$this->template->order_desc = $order_desc;
		$this->template->users = $this->database->table('user')
			->joinWhere(':character', true)
			->order($order_column . ' ' . ($order_desc ? 'DESC' : 'ASC'))
			->select('user.*, :character.id character_id, :character.name character_name');
	}

	public function renderShow($userId)
	{
		if (!$this->getUser()->isInRole('admin') && $this->getUser()->getId() != $userId) {
			$this->flashMessage('Nemáte oprávnění k této stránce', 'warning');
			$this->redirect('Homepage:default');
		}

		$user = $this->database->table('user')
			->joinWhere(':character', '1')
			->wherePrimary($userId)
			->select('user.*, :character.id character_id, :character.name character_name')
			->fetch();

		if (!$user) {
			$this->error('Uživatel nebyl nalezen!');
		}

		$this->template->_user = $user;
	}

	public function actionCreate()
	{
		if (!$this->getUser()->isInRole('admin')) {
			$this->flashMessage('Nemáte oprávnění k této stránce', 'warning');
			$this->redirect('Homepage:default');
		}
	}

	public function actionEdit($userId)
	{
		if (!$this->getUser()->isInRole('admin') && $this->getUser()->getId() != $userId) {
			$this->flashMessage('Nemáte oprávnění k této stránce', 'warning');
			$this->redirect('Homepage:default');
		}
		$this->template->userId = $userId;
		$user = $this->database->table('user')
			->joinWhere(':character', '1')
			->wherePrimary($userId)
			->select('user.*, :character.id character_id, :character.name character_name')
			->fetch();
		if (!$user) {
			$this->error('Uživatel nebyl nalezen!');
		}
		$this->template->_user = $user;
		$this['userForm']->setDefaults($user->toArray());
	}

	public function actionDelete($userId)
	{
		$this->template->userId = $userId;
		$user = $this->database->table('user')->get($userId);
		if ($this->getUser()->getId() == $userId) {
			$this->error('Nelze smazat přihlášeného uživatele');
		}
		if (!$user) {
			$this->error('Uživatel nebyl nalezen');
		}
		$user->delete();
		$this->flashMessage('Uživatel byl smazán', 'success');
		$this->redirect('list');
	}

	protected function createComponentUserForm()
	{
		if (!$this->getUser()->isLoggedIn()) {
			$this->error('Musíte se přihlásit');
		}

		$form = new Form;
		$form->addText('name', 'Jméno a příjmení:')
			->setAttribute('placeholder', 'např. Jan Novák')
			->setRequired();

		$form->addText('nickname', 'Přezdívka:')
			->setDefaultValue('?')
			->setRequired();

		$form->addEmail('email', 'E-mail:')
			->setAttribute('placeholder', 'např. jan.novak@example.com');

		$form->addPassword('password', 'Heslo:')
			->setAttribute('autocomplete', 'new-password')
			->setAttribute('placeholder', 'nevyplníte-li toto pole, heslo se nezmění')
			->setDefaultValue(null);

		if ($this->getUser()->isInRole('admin')) {
			$form->addSelect('role', 'Oprávnění:', $this->_role);
		}
		
		$form->addSelect('continuing', 'Navazující role', array(
			0 => 'hraji novou postavu',
			1 => 'navazuji na svou postavu z předchozího LARPu Florie',
		));

		$form->addSubmit('send', 'Uložit');
		$form->onSuccess[] = [$this, 'userFormSucceeded'];

		return $form;
	}


	public function userFormSucceeded($form, $values)
	{
		$userId = $this->getParameter('userId');

		if (is_null($values['password']) || $values['password'] == '') {
			unset($values['password']);
		} else {
			$values['password'] = password_hash($values['password'], PASSWORD_BCRYPT);
		}

		if ($userId) {
			$user = $this->database->table('user')->get($userId);
			$user->update($values);
		} else {
			$user = $this->database->table('user')->insert($values);
		}

		$this->flashMessage('Údaje o hráči byly uloženy', 'success');
		$this->redirect('show', $user->id);
	}
}
