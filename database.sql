SET NAMES utf8;
SET time_zone = '+00:00';
SET foreign_key_checks = 0;
SET sql_mode = 'NO_AUTO_VALUE_ON_ZERO';

DROP TABLE IF EXISTS `character`;
CREATE TABLE `character` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(255) COLLATE utf16_czech_ci NOT NULL,
  `description` text COLLATE utf16_czech_ci,
  `user_characteristics` text COLLATE utf16_czech_ci,
  `user_note` text COLLATE utf16_czech_ci,
  `admin_note` text COLLATE utf16_czech_ci,
  `user_id` int(11) DEFAULT NULL,
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `updated_at` timestamp NULL DEFAULT NULL ON UPDATE CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`),
  UNIQUE KEY `user_id` (`user_id`),
  CONSTRAINT `character_ibfk_1` FOREIGN KEY (`user_id`) REFERENCES `user` (`id`) ON DELETE SET NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf16 COLLATE=utf16_czech_ci;


DROP TABLE IF EXISTS `group`;
CREATE TABLE `group` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(255) COLLATE utf16_czech_ci NOT NULL,
  `description` text COLLATE utf16_czech_ci,
  `admin_note` text COLLATE utf16_czech_ci,
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `updated_at` timestamp NULL DEFAULT NULL ON UPDATE CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf16 COLLATE=utf16_czech_ci;


DROP TABLE IF EXISTS `group_x_character`;
CREATE TABLE `group_x_character` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `group_id` int(11) NOT NULL,
  `character_id` int(11) NOT NULL,
  `description` text COLLATE utf16_czech_ci,
  `admin_note` text COLLATE utf16_czech_ci,
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `updated_at` timestamp NULL DEFAULT NULL ON UPDATE CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`),
  KEY `group_id` (`group_id`),
  KEY `character_id` (`character_id`),
  CONSTRAINT `group_x_character_ibfk_1` FOREIGN KEY (`group_id`) REFERENCES `group` (`id`) ON DELETE CASCADE,
  CONSTRAINT `group_x_character_ibfk_2` FOREIGN KEY (`character_id`) REFERENCES `character` (`id`) ON DELETE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf16 COLLATE=utf16_czech_ci;


DROP TABLE IF EXISTS `tail`;
CREATE TABLE `tail` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(255) COLLATE utf16_czech_ci NOT NULL,
  `description` text COLLATE utf16_czech_ci,
  `admin_note` text COLLATE utf16_czech_ci,
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `updated_at` timestamp NULL DEFAULT NULL ON UPDATE CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf16 COLLATE=utf16_czech_ci;


DROP TABLE IF EXISTS `tail_x_character`;
CREATE TABLE `tail_x_character` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `tail_id` int(11) NOT NULL,
  `character_id` int(11) NOT NULL,
  `description` text COLLATE utf16_czech_ci,
  `admin_note` text COLLATE utf16_czech_ci,
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `updated_at` timestamp NULL DEFAULT NULL ON UPDATE CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`),
  KEY `tail_id` (`tail_id`),
  KEY `character_id` (`character_id`),
  CONSTRAINT `tail_x_character_ibfk_1` FOREIGN KEY (`tail_id`) REFERENCES `tail` (`id`) ON DELETE CASCADE,
  CONSTRAINT `tail_x_character_ibfk_2` FOREIGN KEY (`character_id`) REFERENCES `character` (`id`) ON DELETE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf16 COLLATE=utf16_czech_ci;


DROP TABLE IF EXISTS `user`;
CREATE TABLE `user` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(255) COLLATE utf16_czech_ci NOT NULL,
  `nickname` varchar(255) COLLATE utf16_czech_ci NOT NULL DEFAULT '?',
  `email` varchar(255) COLLATE utf16_czech_ci NOT NULL,
  `password` varchar(255) COLLATE utf16_czech_ci DEFAULT NULL,
  `role` varchar(255) COLLATE utf16_czech_ci NOT NULL DEFAULT 'player',
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `last_login` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf16 COLLATE=utf16_czech_ci;
