-- Adminer 4.7.1 MySQL dump

SET NAMES utf8;
SET time_zone = '+00:00';
SET foreign_key_checks = 0;
SET sql_mode = 'NO_AUTO_VALUE_ON_ZERO';

SET NAMES utf8mb4;

CREATE DATABASE `florie` /*!40100 DEFAULT CHARACTER SET utf8mb4 */;
USE `florie`;

CREATE TABLE `acquaintance` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `character_id` int(11) NOT NULL,
  `object_id` int(11) NOT NULL,
  `description` text,
  `admin_note` text,
  PRIMARY KEY (`id`),
  UNIQUE KEY `character_id_object_id` (`character_id`,`object_id`),
  KEY `object_id` (`object_id`),
  CONSTRAINT `acquaintance_ibfk_2` FOREIGN KEY (`object_id`) REFERENCES `character` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `acquaintance_ibfk_3` FOREIGN KEY (`character_id`) REFERENCES `character` (`id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;


CREATE TABLE `character` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(255) COLLATE utf16_czech_ci NOT NULL,
  `user_characteristics` text COLLATE utf16_czech_ci,
  `user_note` text COLLATE utf16_czech_ci,
  `admin_note` text COLLATE utf16_czech_ci,
  `user_id` int(11) DEFAULT NULL,
  `sequence` int(11) DEFAULT NULL,
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `updated_at` timestamp NULL DEFAULT NULL ON UPDATE CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`),
  UNIQUE KEY `user_id` (`user_id`),
  CONSTRAINT `character_ibfk_1` FOREIGN KEY (`user_id`) REFERENCES `user` (`id`) ON DELETE SET NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf16 COLLATE=utf16_czech_ci;


CREATE TABLE `tail` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(255) COLLATE utf16_czech_ci NOT NULL,
  `description` text COLLATE utf16_czech_ci,
  `admin_note` text COLLATE utf16_czech_ci,
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `updated_at` timestamp NULL DEFAULT NULL ON UPDATE CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf16 COLLATE=utf16_czech_ci;


CREATE TABLE `tail_x_character` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `tail_id` int(11) NOT NULL,
  `character_id` int(11) NOT NULL,
  `caption` text COLLATE utf16_czech_ci,
  `description` text COLLATE utf16_czech_ci,
  `admin_note` text COLLATE utf16_czech_ci,
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `updated_at` timestamp NULL DEFAULT NULL ON UPDATE CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`),
  KEY `tail_id` (`tail_id`),
  KEY `character_id` (`character_id`),
  CONSTRAINT `tail_x_character_ibfk_1` FOREIGN KEY (`tail_id`) REFERENCES `tail` (`id`) ON DELETE CASCADE,
  CONSTRAINT `tail_x_character_ibfk_2` FOREIGN KEY (`character_id`) REFERENCES `character` (`id`) ON DELETE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf16 COLLATE=utf16_czech_ci;


CREATE TABLE `user` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(255) COLLATE utf16_czech_ci NOT NULL,
  `nickname` varchar(255) COLLATE utf16_czech_ci NOT NULL DEFAULT '?',
  `email` varchar(255) COLLATE utf16_czech_ci NOT NULL,
  `password` varchar(255) COLLATE utf16_czech_ci DEFAULT NULL,
  `role` varchar(255) COLLATE utf16_czech_ci NOT NULL DEFAULT 'player',
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `last_login` timestamp NULL DEFAULT NULL,
  `continuing` tinyint(1) NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf16 COLLATE=utf16_czech_ci;


-- 2019-07-10 17:21:12
